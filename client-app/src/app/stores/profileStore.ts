import { action, computed, observable, reaction, runInAction } from "mobx";
import { toast } from "react-toastify";
import agent from "../api/agent";
import { IPhoto, IProfile, IUserActivity } from "../models/profile";
import { RootStore } from "./rootStore";

export default class ProfileStore {
    rootStore: RootStore;

    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;

        reaction(
            () => this.activeTab,
            activeTab => {
                if (activeTab === 3 || activeTab === 4) {
                    const predicate = activeTab === 3 ? "followers" : "following";
                    this.loadFollowings(predicate);
                } else {
                    this.followings = [];
                }
            }
        )
    }

    @observable profile: IProfile | null = null;
    @observable loadingProfile: boolean = false;
    @observable uploadingPhoto: boolean = false;
    @observable loading: boolean = false;
    @observable followings: IProfile[] = [];
    @observable activeTab: number = 0;
    @observable userActivities: IUserActivity[] = [];
    @observable loadingActivities: boolean = false;

    @computed get isCurrentUser() {
        if (this.rootStore.userStore.user && this.profile) {
            return this.rootStore.userStore.user.username === this.profile.username;
        } else {
            return false;
        }
    }

    @action loadUserActivities = async (username: string, predicate?: string) => {
        this.loadingActivities = true;
        try {
            const activities = await agent.Profiles.listActivities(username, predicate!);
            runInAction("loadUserActitivies fail", () => {
                this.userActivities = activities;
                this.loadingActivities = false;
            })
        }
        catch(error) {
            toast.error("Problem loading activities");
            runInAction("loadUserActitivies fail", () => {
                this.loadingActivities = false;
            });
        }
    }

    @action setActiveTab = (activeIndex: number) => {
        this.activeTab = activeIndex;
    }

    @action loadProfile = async (username: string) => {
        this.loadingProfile = true;
        try
        {
            const usernameProfile = await agent.Profiles.get(username);
            runInAction('load profile success', () => {
                this.profile = usernameProfile;
                this.loadingProfile = false;
              });
        }catch (error)
        {
            runInAction('load profile fail', () => {
                this.loadingProfile = false;
              });
            console.log(error);
        }
    }

    @action editProfile = async (profile: IProfile) => {
        try
        {
            await agent.Profiles.update(profile);
            runInAction("Updating profile success", () => {
                this.profile!.displayName = profile.displayName;
                this.profile!.bio = profile.bio;
                this.rootStore.userStore.user!.displayName = profile.displayName;
            });

        }
        catch(error)
        {
            console.log(error);
            toast.error("Problem updating the profile")
        }
    }

    @action uploadPhoto = async (file: Blob) => {
        this.uploadingPhoto = true;
        try
        {
            const photo = await agent.Profiles.uploadPhoto(file);
            runInAction("upload photo success", () => {
                if (this.profile) {
                    this.profile.photos.push(photo);
                    if (photo.isMain && this.rootStore.userStore.user) {
                        this.rootStore.userStore.user.image = photo.url;
                        this.profile.image = photo.url;
                    }
                    this.uploadingPhoto = false;
                }
            })
        }
        catch(error)
        {
            console.log(error);
            toast.error("Problem uploading photo");
            runInAction("upload photo fail", () => {
                this.uploadingPhoto = false;
            })
        }
    }

    @action setMainPhoto = async (photo: IPhoto) => {
        this.loading = true;
        try
        {
            await agent.Profiles.setMainPhoto(photo.id);
            runInAction("setMainPhoto success",() => {
                this.rootStore.userStore.user!.image = photo.url;
                this.profile!.photos.find(i => i.isMain)!.isMain = false;
                this.profile!.photos.find(i => i.id === photo.id)!.isMain = true;
                this.profile!.image = photo.url;
                this.loading = false;
            })
        }
        catch(error)
        {
            toast.error("Problem setting photo as main")
            runInAction("setMainPhoto fail",() => {
                this.loading = false;
            })
        }
    }

    @action deletePhoto = async (photo: IPhoto) => {
        this.loading = true;
        try
        {
            await agent.Profiles.deletePhoto(photo.id);
            runInAction("deletePhoto success",() => {
                this.profile!.photos = this.profile!.photos.filter(p => p.id !== photo.id);
                this.loading = false;
            })
        }
        catch(error)
        {
            toast.error("Problem deleting the photo")
            runInAction("deletePhoto fail",() => {
                this.loading = false;
            })
        }
    }

    @action follow = async (username: string) => {
        this.loading = true;
        try
        {
            await agent.Profiles.follow(username);
            runInAction("follow success",() => {
                this.profile!.following = true;
                this.profile!.followersCount++;
                this.loading = false;
            })
        }
        catch(error)
        {
            toast.error("Problem following the user")
            runInAction("follow fail",() => {
                this.loading = false;
            })
        }
    }

    @action unfollow = async (username: string) => {
        this.loading = true;
        try
        {
            await agent.Profiles.unfollow(username);
            runInAction("unfollow success",() => {
                this.profile!.following = false;
                this.profile!.followersCount--;
                this.loading = false;
            })
        }
        catch(error)
        {
            toast.error("Problem unfollowing the user")
            runInAction("unfollow fail",() => {
                this.loading = false;
            })
        }
    }

    @action loadFollowings = async (predicate: string) => {
        this.loading = true;
        try
        {
            const profiles = await agent.Profiles.listFollowings(this.profile!.username, predicate);
            runInAction("loadFollowings success",() => {
                this.followings = profiles;
                this.loading = false;
            })
        }
        catch(error)
        {
            toast.error("Problem loading followings")
            runInAction("loadFollowings fail",() => {
                this.loading = false;
            })
        }
    }
}