import { observable, action, computed, runInAction, reaction } from 'mobx';
import { toast } from 'react-toastify';
import { history } from '../..';
import agent from '../api/agent';
import { createAttendee, setActivityProps } from '../common/util/util';
import { IActivity, IAttendee } from '../models/activity';
import { RootStore } from './rootStore';
import {HubConnection, HubConnectionBuilder, HubConnectionState, LogLevel} from "@microsoft/signalr";

const LIMIT = 2;

export default class ActivityStore {
  rootStore: RootStore;
  
  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;

    reaction(
      () => this.predicate.keys(),
      () => {
        this.page = 0;
        this.activityRegistry.clear();
        this.loadActivities();
      }
    );
  }

  @observable activityRegistry = new Map();
  @observable loadingInitial = false;
  @observable activity: IActivity | null = null;
  @observable submitting = false;
  @observable target = '';
  @observable loading = false;
  @observable.ref hubConnection: HubConnection | null = null;
  @observable activityCount = 0;
  @observable page = 0;
  @observable predicate = new Map();

  @action setPredicate = (predicate: string, value: string | Date) => {
    this.predicate.clear();
    if (predicate !== "all") {
      this.predicate.set(predicate, value);
    }
  }

  @computed get axiosParams() {
    const params = new URLSearchParams();
    params.append('limit', String(LIMIT));
    params.append("offset", `${this.page ? this.page * LIMIT : 0}`);
    this.predicate.forEach((value, key) => {
      if (key==="startDate") {
        params.append(key, value.toISOString());
      } else {
        params.append(key, value);
      }
    })
    return params;
  }

  @computed get totalPages() {
    return Math.ceil(this.activityCount/LIMIT);
  }

  @action setPage = (page: number) => {
    this.page = page;
  }

  @action createHubConnection = async (activityId: string) => {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl("http://localhost:5000/chat", {
        accessTokenFactory: () => this.rootStore.commonStore.token!
      })
      .configureLogging(LogLevel.Information)
      .build();

    await this.hubConnection.start();
    console.log(this.hubConnection.state);
    
    if (this.hubConnection.state === HubConnectionState.Connected) {
      console.log("Attempting to join group");
      this.hubConnection!.invoke("AddToGroup", activityId)
        .then(() => console.log("Added to group"))
        .catch(error => console.log(error));

      // The client will execute this callback when it receives the command "ReceiveComment"
      this.hubConnection.on("ReceiveComment", comment => {
        runInAction("ReceiveComment action", () => {
          this.activity!.comments.push(comment)
        });
      });

      this.hubConnection.on("Send", message => {
        toast.info(message);
      })
    }
    else {
      console.log("Connection not established");
    }
  }

  @action stopHubConnection = () => {
    this.hubConnection!.invoke("RemoveFromGroup", this.activity!.id)
    .then(() => {
      this.hubConnection!.stop();
    })
    .then(() => console.log("Connection stopped"))
    .catch(error => console.log(error));
  }

  @action addComment = async (values: any) => {
    values.activityId = this.activity!.id;
    try {
      // Invokes this method in the server with the parameters
      await this.hubConnection!.invoke("SendComment", values);
    }
    catch (error) {

    }
  }

  @computed get activitiesByDate() {
    return this.groupActivitiesByDate(
      Array.from(this.activityRegistry.values())
    );
  }

  groupActivitiesByDate(activities: IActivity[]) {
    var sortedActivities = activities.sort(
      (a, b) => a.date.getTime() - b.date.getTime()
    );
    return Object.entries(
      sortedActivities.reduce((activities, activity) => {
        var date = activity.date.toISOString().split('T')[0];
        activities[date] = activities[date]
          ? [...activities[date], activity]
          : [activity];
        return activities;
      }, {} as { [key: string]: IActivity[] })
    );
  }

  @action loadActivities = async () => {
    this.loadingInitial = true;

    try {
      var activitiesEnvelope = await agent.Activities.list(this.axiosParams);
      const {activities, activityCount} = activitiesEnvelope;
      runInAction('load activities success', () => {
        if (activities) {
          activities.forEach((activity) => {
            activity = setActivityProps(activity, this.rootStore.userStore.user!);
            this.activityRegistry.set(activity.id, activity);
          });
          this.activityCount = activityCount;
        }
        this.loadingInitial = false;
      });
    } catch (err) {
      console.log(err);
      runInAction('load activities fail', () => {
        this.loadingInitial = false;
      });
    }
  };

  @action loadActivity = async (id: string) => {
    let activity = this.getActivity(id);

    if (activity) {
      this.activity = activity;
      return activity;
    } else {
      this.loadingInitial = true;
      try {
        activity = await agent.Activities.details(id);
        runInAction('get activity success', () => {
          activity = setActivityProps(activity, this.rootStore.userStore.user!);
          this.activity = activity;
          this.activityRegistry.set(activity.id, activity);
          this.loadingInitial = false;
        });
        return activity;
      } catch (err) {
        console.log(err);
        runInAction('get activity fail', () => {
          this.loadingInitial = false;
        });
      }
    }
  };

  getActivity = (id: string) => {
    return this.activityRegistry.get(id);
  };

  @action clearActivity = () => {
    this.activity = null;
  };

  @action createActivity = async (activity: IActivity) => {
    this.submitting = true;
    try {
      await agent.Activities.create(activity);
      const attendee = createAttendee(this.rootStore.userStore.user!);
      attendee.isHost = true;
      let attendees: IAttendee[] = [];
      attendees.push(attendee);
      activity.attendees = attendees;
      activity.comments = [];
      activity.isHost = true;
      runInAction('create activity success', () => {
        this.activity = activity;
        this.activityRegistry.set(activity.id, this.activity);
        this.submitting = false;
      });
      history.push(`/activities/${activity.id}`);
    } catch (err) {
      runInAction('create activity success', () => {
        this.submitting = false;
      });
      toast.error('Problem submitting data');
      console.log(err.response);
    }
  };

  @action editActivity = async (activity: IActivity) => {
    this.submitting = true;
    try {
      await agent.Activities.update(activity);
      runInAction('edit activity success', () => {
        this.activityRegistry.set(activity.id, activity);
        this.activity = activity;
        this.submitting = false;
      });
      history.push(`/activities/${activity.id}`);
    } catch (err) {
      runInAction('edit activity success', () => {
        this.submitting = false;
      });
      toast.error('Problem submitting data');
      console.log(err.response);
    }
  };

  @action deleteActivity = async (target: string, id: string) => {
    this.submitting = true;
    this.target = target;
    try {
      await agent.Activities.delete(id);
      runInAction('delete activity success', () => {
        this.activityRegistry.delete(id);
        this.submitting = false;
        this.target = '';
      });
    } catch (err) {
      console.log(err);
      runInAction('delete activity fail', () => {
        this.submitting = false;
        this.target = '';
      });
    }
  };

  @action attendActivity = async () => {
    const attendee = createAttendee(this.rootStore.userStore.user!);
    this.loading = true;

    try{
      await agent.Activities.attend(this.activity!.id);
      runInAction('attend activity success', () => {
        if (this.activity) {
          this.activity.attendees.push(attendee);
          this.activity.isGoing = true;
          this.activityRegistry.set(this.activity.id, this.activity);
          this.loading = false;
        }
      })
    }
    catch(error)
    {
      runInAction('attend activitie fail', () => {
        this.loading = false;
      });
      toast.error('Problem signing up to activity');
    }
  }

  @action cancelAttendance = async () => {
    const user = this.rootStore.userStore.user!;
    this.loading = true;

    try{
      await agent.Activities.unattend(this.activity!.id);
      runInAction('cancel attendance success', () => {
        if (this.activity) {
          this.activity.attendees = this.activity.attendees.filter(a => a.username !== user.username)
          this.activity.isGoing = false;
          this.activityRegistry.set(this.activity.id, this.activity);
          this.loading = false;
        }
      });
    }
    catch(error)
    {
      runInAction('cancel attendance fail', () => {
        this.loading = false;
      });
      toast.error('Problem signing off to activity');
    }

    
  }
}
