import React from "react";
import { List, Image, Popup } from "semantic-ui-react";
import { IAttendee } from "../../../app/models/activity";

interface IProps {
  attendees: IAttendee[];
}

const styles = {
  borderColor: "Orange",
  borderWidth: 2,
};

const ActivityListItemAttendees: React.FC<IProps> = ({ attendees }) => {
  return (
    <List horizontal>
      {attendees.map((a) => {
        return (
          <List.Item key={a.username}>
            <Popup
              header={a.displayName}
              trigger={
                <Image
                  size="mini"
                  circular
                  src={a.image || "/assets/user.png"}
                  bordered
                  style={a.following ? styles : null}
                />
              }
            />
          </List.Item>
        );
      })}
    </List>
  );
};

export default ActivityListItemAttendees;
